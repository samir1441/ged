package org.geseld.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.math.BigInteger;


/**
 * The persistent class for the document database table.
 * 
 */
@Entity
@Table(name="document")
@NamedQuery(name="Document.findAll", query="SELECT d FROM Document d")
public class Document implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_DOCUMENT", unique=true, nullable=false)
	private int idDocument;

	@Lob
	@Column(name="CHEMIN_FICHIER_DOCUMENT", nullable=false)
	private String cheminFichierDocument;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATE_HEURE_STATUT_DOCUMENT", nullable=false)
	private Date dateHeureStatutDocument;

	@Lob
	@Column(name="DESCRIPTION_DOCUMENT")
	private String descriptionDocument;

	@Lob
	@Column(name="LIBELLE_DOCUMENT", nullable=false)
	private String libelleDocument;

	@Column(name="TAILLE_DOCUMENT", nullable=false)
	private BigInteger tailleDocument;

	//uni-directional many-to-one association to TypeDocument
	@ManyToOne
	@JoinColumn(name="ID_TYPE_DOCUMENT")
	private TypeDocument typeDocument;

	//uni-directional many-to-one association to Statut
	@ManyToOne
	@JoinColumn(name="ID_STATUT", nullable=false)
	private Statut statut;

	//bi-directional many-to-one association to Conteneur
	@ManyToOne
	@JoinColumn(name="ID_CONTENEUR", nullable=false)
	private Conteneur conteneur;

	public Document() {
	}

	public int getIdDocument() {
		return this.idDocument;
	}

	public void setIdDocument(int idDocument) {
		this.idDocument = idDocument;
	}

	public String getCheminFichierDocument() {
		return this.cheminFichierDocument;
	}

	public void setCheminFichierDocument(String cheminFichierDocument) {
		this.cheminFichierDocument = cheminFichierDocument;
	}

	public Date getDateHeureStatutDocument() {
		return this.dateHeureStatutDocument;
	}

	public void setDateHeureStatutDocument(Date dateHeureStatutDocument) {
		this.dateHeureStatutDocument = dateHeureStatutDocument;
	}

	public String getDescriptionDocument() {
		return this.descriptionDocument;
	}

	public void setDescriptionDocument(String descriptionDocument) {
		this.descriptionDocument = descriptionDocument;
	}

	public String getLibelleDocument() {
		return this.libelleDocument;
	}

	public void setLibelleDocument(String libelleDocument) {
		this.libelleDocument = libelleDocument;
	}

	public BigInteger getTailleDocument() {
		return this.tailleDocument;
	}

	public void setTailleDocument(BigInteger tailleDocument) {
		this.tailleDocument = tailleDocument;
	}

	public TypeDocument getTypeDocument() {
		return this.typeDocument;
	}

	public void setTypeDocument(TypeDocument typeDocument) {
		this.typeDocument = typeDocument;
	}

	public Statut getStatut() {
		return this.statut;
	}

	public void setStatut(Statut statut) {
		this.statut = statut;
	}

	public Conteneur getConteneur() {
		return this.conteneur;
	}

	public void setConteneur(Conteneur conteneur) {
		this.conteneur = conteneur;
	}

}