package org.geseld.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the conteneur database table.
 * 
 */
@Entity
@Table(name="conteneur")
@NamedQuery(name="Conteneur.findAll", query="SELECT c FROM Conteneur c")
public class Conteneur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_CONTENEUR", unique=true, nullable=false)
	private String idConteneur;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATE_HEURE", nullable=false)
	private Date dateHeure;

	@Lob
	@Column(name="LIBELLE_CONTENEUR", nullable=false)
	private String libelleConteneur;

	//uni-directional many-to-one association to EmplacementPhysique
	@ManyToOne
	@JoinColumn(name="ID_EMPLACEMENT", nullable=false)
	private EmplacementPhysique emplacementPhysique;

	//bi-directional many-to-one association to Bibliotheque
	@ManyToOne
	@JoinColumn(name="ID_BIBLIOTHEQUE")
	private Bibliotheque bibliotheque;

	//bi-directional many-to-one association to Conteneur
	@ManyToOne
	@JoinColumn(name="ID_CONTENEUR_PARENT")
	private Conteneur conteneurParent;

	//bi-directional many-to-one association to Conteneur
	@OneToMany(mappedBy="conteneurParent")
	private List<Conteneur> conteneursFils;

	//bi-directional many-to-one association to Document
	@OneToMany(mappedBy="conteneur")
	private List<Document> documents;

	public Conteneur() {
	}

	public String getIdConteneur() {
		return this.idConteneur;
	}

	public void setIdConteneur(String idConteneur) {
		this.idConteneur = idConteneur;
	}

	public Date getDateHeure() {
		return this.dateHeure;
	}

	public void setDateHeure(Date dateHeure) {
		this.dateHeure = dateHeure;
	}

	public String getLibelleConteneur() {
		return this.libelleConteneur;
	}

	public void setLibelleConteneur(String libelleConteneur) {
		this.libelleConteneur = libelleConteneur;
	}

	public EmplacementPhysique getEmplacementPhysique() {
		return this.emplacementPhysique;
	}

	public void setEmplacementPhysique(EmplacementPhysique emplacementPhysique) {
		this.emplacementPhysique = emplacementPhysique;
	}

	public Bibliotheque getBibliotheque() {
		return this.bibliotheque;
	}

	public void setBibliotheque(Bibliotheque bibliotheque) {
		this.bibliotheque = bibliotheque;
	}

	public Conteneur getConteneurParent() {
		return this.conteneurParent;
	}

	public void setConteneurParent(Conteneur conteneurParent) {
		this.conteneurParent = conteneurParent;
	}

	public List<Conteneur> getConteneursFils() {
		return this.conteneursFils;
	}

	public void setConteneursFils(List<Conteneur> conteneursFils) {
		this.conteneursFils = conteneursFils;
	}

	public Conteneur addConteneursFil(Conteneur conteneursFil) {
		getConteneursFils().add(conteneursFil);
		conteneursFil.setConteneurParent(this);

		return conteneursFil;
	}

	public Conteneur removeConteneursFil(Conteneur conteneursFil) {
		getConteneursFils().remove(conteneursFil);
		conteneursFil.setConteneurParent(null);

		return conteneursFil;
	}

	public List<Document> getDocuments() {
		return this.documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public Document addDocument(Document document) {
		getDocuments().add(document);
		document.setConteneur(this);

		return document;
	}

	public Document removeDocument(Document document) {
		getDocuments().remove(document);
		document.setConteneur(null);

		return document;
	}

}