package org.geseld.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the action database table.
 * 
 */
@Entity
@Table(name="action")
@NamedQuery(name="Action.findAll", query="SELECT a FROM Action a")
public class Action implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_ACTION", unique=true, nullable=false)
	private short idAction;

	@Lob
	@Column(name="LIBELLE_ACTION", nullable=false)
	private String libelleAction;

	public Action() {
	}

	public short getIdAction() {
		return this.idAction;
	}

	public void setIdAction(short idAction) {
		this.idAction = idAction;
	}

	public String getLibelleAction() {
		return this.libelleAction;
	}

	public void setLibelleAction(String libelleAction) {
		this.libelleAction = libelleAction;
	}

}