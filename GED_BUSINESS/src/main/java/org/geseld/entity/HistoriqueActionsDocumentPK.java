package org.geseld.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the historique_actions_documents database table.
 * 
 */
@Embeddable
public class HistoriqueActionsDocumentPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ID_ACTION", insertable=false, updatable=false, unique=true, nullable=false)
	private short idAction;

	@Column(name="ID_UTILISATEUR", insertable=false, updatable=false, unique=true, nullable=false)
	private String idUtilisateur;

	@Column(name="ID_DOCUMENT", insertable=false, updatable=false, unique=true, nullable=false)
	private int idDocument;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATE_HEURE_ACTION", insertable=false, updatable=false, unique=true, nullable=false)
	private java.util.Date dateHeureAction;

	public HistoriqueActionsDocumentPK() {
	}
	public short getIdAction() {
		return this.idAction;
	}
	public void setIdAction(short idAction) {
		this.idAction = idAction;
	}
	public String getIdUtilisateur() {
		return this.idUtilisateur;
	}
	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public int getIdDocument() {
		return this.idDocument;
	}
	public void setIdDocument(int idDocument) {
		this.idDocument = idDocument;
	}
	public java.util.Date getDateHeureAction() {
		return this.dateHeureAction;
	}
	public void setDateHeureAction(java.util.Date dateHeureAction) {
		this.dateHeureAction = dateHeureAction;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof HistoriqueActionsDocumentPK)) {
			return false;
		}
		HistoriqueActionsDocumentPK castOther = (HistoriqueActionsDocumentPK)other;
		return 
			(this.idAction == castOther.idAction)
			&& this.idUtilisateur.equals(castOther.idUtilisateur)
			&& (this.idDocument == castOther.idDocument)
			&& this.dateHeureAction.equals(castOther.dateHeureAction);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) this.idAction);
		hash = hash * prime + this.idUtilisateur.hashCode();
		hash = hash * prime + this.idDocument;
		hash = hash * prime + this.dateHeureAction.hashCode();
		
		return hash;
	}
}