package org.geseld.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the statut database table.
 * 
 */
@Entity
@Table(name="statut")
@NamedQuery(name="Statut.findAll", query="SELECT s FROM Statut s")
public class Statut implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_STATUT", unique=true, nullable=false)
	private short idStatut;

	@Column(name="CODE_STATUT", nullable=false)
	private short codeStatut;

	@Lob
	@Column(name="LIBELLE_STATUT", nullable=false)
	private String libelleStatut;

	public Statut() {
	}

	public short getIdStatut() {
		return this.idStatut;
	}

	public void setIdStatut(short idStatut) {
		this.idStatut = idStatut;
	}

	public short getCodeStatut() {
		return this.codeStatut;
	}

	public void setCodeStatut(short codeStatut) {
		this.codeStatut = codeStatut;
	}

	public String getLibelleStatut() {
		return this.libelleStatut;
	}

	public void setLibelleStatut(String libelleStatut) {
		this.libelleStatut = libelleStatut;
	}

}