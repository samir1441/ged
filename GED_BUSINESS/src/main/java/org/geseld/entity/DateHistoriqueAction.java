package org.geseld.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the date_historique_action database table.
 * 
 */
@Entity
@Table(name="date_historique_action")
@NamedQuery(name="DateHistoriqueAction.findAll", query="SELECT d FROM DateHistoriqueAction d")
public class DateHistoriqueAction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DATE_HEURE_ACTION", unique=true, nullable=false)
	private Date dateHeureAction;

	public DateHistoriqueAction() {
	}

	public Date getDateHeureAction() {
		return this.dateHeureAction;
	}

	public void setDateHeureAction(Date dateHeureAction) {
		this.dateHeureAction = dateHeureAction;
	}

}