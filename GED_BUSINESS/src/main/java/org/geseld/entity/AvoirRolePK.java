package org.geseld.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the avoir_role database table.
 * 
 */
@Embeddable
public class AvoirRolePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ID_UTILISATEUR", insertable=false, updatable=false, unique=true, nullable=false)
	private String idUtilisateur;

	@Column(name="ID_BIBLIOTHEQUE", insertable=false, updatable=false, unique=true, nullable=false)
	private String idBibliotheque;

	@Column(name="ID_ROLE", insertable=false, updatable=false, unique=true, nullable=false)
	private String idRole;

	public AvoirRolePK() {
	}
	public String getIdUtilisateur() {
		return this.idUtilisateur;
	}
	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public String getIdBibliotheque() {
		return this.idBibliotheque;
	}
	public void setIdBibliotheque(String idBibliotheque) {
		this.idBibliotheque = idBibliotheque;
	}
	public String getIdRole() {
		return this.idRole;
	}
	public void setIdRole(String idRole) {
		this.idRole = idRole;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AvoirRolePK)) {
			return false;
		}
		AvoirRolePK castOther = (AvoirRolePK)other;
		return 
			this.idUtilisateur.equals(castOther.idUtilisateur)
			&& this.idBibliotheque.equals(castOther.idBibliotheque)
			&& this.idRole.equals(castOther.idRole);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idUtilisateur.hashCode();
		hash = hash * prime + this.idBibliotheque.hashCode();
		hash = hash * prime + this.idRole.hashCode();
		
		return hash;
	}
}