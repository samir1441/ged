package org.geseld.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the type_document database table.
 * 
 */
@Entity
@Table(name="type_document")
@NamedQuery(name="TypeDocument.findAll", query="SELECT t FROM TypeDocument t")
public class TypeDocument implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_TYPE_DOCUMENT", unique=true, nullable=false)
	private short idTypeDocument;

	@Column(name="CODE_TYPE_DOCUMENT", nullable=false)
	private short codeTypeDocument;

	@Lob
	@Column(name="LIBELLE_TYPE_DOCUMENT", nullable=false)
	private String libelleTypeDocument;

	//uni-directional many-to-one association to TypeDocument
	@ManyToOne
	@JoinColumn(name="ID_SUPER_TYPE")
	private TypeDocument superTypeDocument;

	public TypeDocument() {
	}

	public short getIdTypeDocument() {
		return this.idTypeDocument;
	}

	public void setIdTypeDocument(short idTypeDocument) {
		this.idTypeDocument = idTypeDocument;
	}

	public short getCodeTypeDocument() {
		return this.codeTypeDocument;
	}

	public void setCodeTypeDocument(short codeTypeDocument) {
		this.codeTypeDocument = codeTypeDocument;
	}

	public String getLibelleTypeDocument() {
		return this.libelleTypeDocument;
	}

	public void setLibelleTypeDocument(String libelleTypeDocument) {
		this.libelleTypeDocument = libelleTypeDocument;
	}

	public TypeDocument getSuperTypeDocument() {
		return this.superTypeDocument;
	}

	public void setSuperTypeDocument(TypeDocument superTypeDocument) {
		this.superTypeDocument = superTypeDocument;
	}

}