package org.geseld.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the emplacement_physique database table.
 * 
 */
@Entity
@Table(name="emplacement_physique")
@NamedQuery(name="EmplacementPhysique.findAll", query="SELECT e FROM EmplacementPhysique e")
public class EmplacementPhysique implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_EMPLACEMENT", unique=true, nullable=false)
	private int idEmplacement;

	@Lob
	@Column(name="LIBELLE_EMPLACEMENT", nullable=false)
	private String libelleEmplacement;

	public EmplacementPhysique() {
	}

	public int getIdEmplacement() {
		return this.idEmplacement;
	}

	public void setIdEmplacement(int idEmplacement) {
		this.idEmplacement = idEmplacement;
	}

	public String getLibelleEmplacement() {
		return this.libelleEmplacement;
	}

	public void setLibelleEmplacement(String libelleEmplacement) {
		this.libelleEmplacement = libelleEmplacement;
	}

}