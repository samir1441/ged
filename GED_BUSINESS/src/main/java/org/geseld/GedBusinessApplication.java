package org.geseld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GedBusinessApplication {
// fo penser à installer mysql
	public static void main(String[] args) {
		SpringApplication.run(GedBusinessApplication.class, args);
	}
}
